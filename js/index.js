"use strict"
var info, botm, isClsPicOpen, ReaderInfo//info是弹出拟态框，botm是在底下的，isClsPicOpen是看图片开没开，ReaderInfo是小说的JSON解析;
function init() {//初始化
    info = M.Modal.init(document.getElementById("modal"), {
        onCloseEnd: function () {
            document.getElementById("info_content").innerHTML = '';
        },
    });
    botm = M.Modal.init(document.getElementById("modalb"), {
        onCloseEnd: function () {
            document.getElementById("botm_content").innerHTML = '';
            if (isClsPicOpen) {
                ClsPicOff()
            }
        }
    });
    M.Sidenav.init(document.getElementById("slide-out"), {});
    isClsPicOpen = false;
    window.addEventListener("hashchange", hchanged, false)

    //更改小说阅读列表
    var xhr = new XMLHttpRequest();
    xhr.open("GET", './indexs/reader.json', true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                ReaderInfo = JSON.parse(xhr.responseText);
                var tab2 = '<div class="row">'
                for (var abb in ReaderInfo) {
                    tab2 += '<div class="col s12 m6 l4"><div class="card"><div class="card-image"><img src="img/' + ReaderInfo[abb].pic + '" loading="lazy"><span class="card-title" style="color: ' + ReaderInfo[abb].TitleFontColor + ';">' + ReaderInfo[abb].name + '</span></div><div class="card-content"><p>' + ReaderInfo[abb].info + '</p><p>小说作者：' + ReaderInfo[abb].auther + '；小说贡献协议：' + ReaderInfo[abb].license + '</p></div><div class="card-action"><a href="#character/' + abb + '" onclick="character(\'' + abb + '\')">章节列表</a></div></div> </div>'
                }
                tab2 += '</div>'
                document.getElementById("tab2").innerHTML = tab2;
            } else {
                alert("网络错误，代码" + xhr.status);
            }
        }
    }

    //根据location.hash打开对应的框框和页面
    switch (location.hash) {
        case "#tab/2": {
            ctab('tab2', '小说')
            break;
        }
        case "#tab/3": {
            ctab('tab3', '小游戏')
            break;
        }
        case "#tab/4": {
            ctab('tab4', '关于和聊天')
            break;
        }
        default: {
            if (location.hash.split("/")[0] == "#modal") {
                modal(location.hash.substring(7, location.hash.length));
            } else if (location.hash.split("/")[0] == "#reader") {
                reader(location.hash.substring(8, location.hash.length));
            }else if (location.hash.split("/")[0] == "#character") {
                character(location.hash.substring(11, location.hash.length));
            }
            break;
        }
    }

}

function hchanged() {//当#后面的东西改变了
    console.log(location.hash);
    if (location.hash == "#c") {//关阅读器+关modal框框
        if (isClsPicOpen) {
            ClsPicOff();
        }
        info.close();
    }
}

function modal(node, jscode) { //拟态框页面
    var xhr = new XMLHttpRequest();
    xhr.open("GET", './modals/' + node + '.json', true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                location.hash = "#c"
                location.hash = "#modal/" + node;
                let ans = JSON.parse(xhr.responseText);
                document.getElementById("info_header").innerHTML = ans.header;
                document.getElementById("info_content").innerHTML = ans.content;
                document.getElementById("info_footer").innerHTML = ans.footer;
                info.open();
                if (jscode !== null && jscode !== undefined) { //如果有异步操作
                    jscode.apply();
                }
            } else {
                alert("网络错误，代码" + xhr.status);
            }
        }
    }
}


function SettingSet(name, value) { //设置保存
    if (value === undefined || value === null) {
        localStorage.removeItem(name);
    } else {
        localStorage.setItem(name, value);
    }
}

function SettingCKBLoad(name, id) { //开关设置内容加载
    if (localStorage.getItem(name) == true || localStorage.getItem(name) == "true") {
        document.getElementById(id).checked = "1"
    } else {
        document.getElementById(id).checked = ""
    }
}
function ctab(tab, title) {//动画更换页面
    if (title !== null && title !== undefined && title != "") {
        if (document.getElementById("NavTitle").innerHTML == "") {
            document.getElementById("top-nav").style.display = "block";
            document.getElementById("NavTitle").innerHTML = title;
            document.getElementById("NavTitle").style.animation = "fadein 0.3s forwards";
            setTimeout(function () {
                document.getElementById("NavTitle").style.animation = "";
            }, 301);
        } else {
            document.getElementById("NavTitle").style.animation = "fadeaway 0.3s forwards";
            setTimeout(function () {
                document.getElementById("NavTitle").innerHTML = title;
                document.getElementById("NavTitle").style.animation = "fadein 0.3s forwards";
            }, 301);
            setTimeout(function () {
                document.getElementById("NavTitle").style.animation = "";
            }, 601);
        }
    } else {
        document.getElementById("NavTitle").style.animation = "fadeaway 0.3s forwards";
        setTimeout(function () {
            document.getElementById("NavTitle").style.animation = "";
            document.getElementById("top-nav").style.display = "none";
            document.getElementById("NavTitle").innerHTML = "";
        }, 301);
    }


    document.getElementById("tab1").style.animation = "fadeaway 0.3s forwards";
    document.getElementById("tab2").style.animation = "fadeaway 0.3s forwards";
    document.getElementById("tab3").style.animation = "fadeaway 0.3s forwards";
    document.getElementById("tab4").style.animation = "fadeaway 0.3s forwards";

    setTimeout(function () {
        document.getElementById("tab1").style.display = "none";
        document.getElementById("tab2").style.display = "none";
        document.getElementById("tab3").style.display = "none";
        document.getElementById("tab4").style.display = "none";

        document.getElementById(tab).style.display = "block";
        document.getElementById(tab).style.animation = "fadein 0.3s forwards";
    }, 301);
    setTimeout(function () {
        document.getElementById("tab1").style.animation = "";
        document.getElementById("tab2").style.animation = "";
        document.getElementById("tab3").style.animation = "";
        document.getElementById("tab4").style.animation = "";
    }, 601);
}
function lastread() {//继续上次的阅读
    if (localStorage.getItem("lastread")) {
        reader(localStorage.getItem("lastread"));
    } else {
        alert("看起来你还没有读过呢，试试手气!")
        randomread()
    }
}
function randomread() {//随机阅读
    if(ReaderInfo===undefined){//如果当前还没拿到小说信息
        console.log("系统正加载")
        setTimeout(() => {
            randomread();
        }, 200);
        return;
    }
    let rand=Math.ceil(Math.random() * Object.keys(ReaderInfo).length - 1);//给第一步的随机选择小说篇目
    let character=ReaderInfo[Object.keys(ReaderInfo)[rand]]
    let rand2=Math.ceil(Math.random() * Object.keys(character).length - 1);//给第二步的随机选择小说章节
    reader(Object.keys(ReaderInfo)[rand]+"/"+character.ReaderList[Object.keys(character.ReaderList)[rand2]]);
}

function character(code) {//显示章节的内容
    if(ReaderInfo===undefined){//如果当前还没拿到小说信息
        console.log("系统正加载")
        setTimeout(() => {
            character(code);
        }, 200);
        return;
    }
    let list = ReaderInfo[code].ReaderList;
    let modal = "<div class=\"collection\">"
    for (var abb in list) {
        modal += "<a href=\"#!\" class=\"modal-close collection-item\" onclick=\"reader('"+code+"/"+list[abb]+"')\">"+abb+"</a>"
    }
    modal += '</div>'
    document.getElementById("info_header").innerHTML = ReaderInfo[code].name + "-章节选择";
    document.getElementById("info_content").innerHTML = modal;
    document.getElementById("info_footer").innerHTML = "<a  class=\"modal-close waves-effect waves-green btn-flat\" href=\"#!\">关闭</a>";
    info.open();
}
