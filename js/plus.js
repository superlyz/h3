"use strict"
var ctd;

function clsshow(title, requires, timeset, node, footer) { //进入关卡，json格式为btnfun、btnname分别为方法和按钮名字;如果定义了noclose=true则不用关闭对话框且无视倒计时禁用；如果定义了notime=true则不要管它的限制时间问题
    ctd = window.setInterval(ctdgo, 1000); //启动倒计时
    //向拟态框填充内容
    document.getElementById("botm_header").innerHTML = title;
    document.getElementById("botm_content").innerHTML = requires + "<p id=\"CountDown\">距离结束还有" + timeset + "秒</p>";
    let t=0;
    var footers="";
    if(timeset<=0){
        while(t<footer.length){
            if(footer[t]['noclose']==true || footer['noclose']=="true"){
                footers+='<button href="#!" class="waves-effect waves-green btn-flat" onclick="'+footer[t]["btnfun"]+'">'+footer[t]["btnname"]+'</button>'
            }else{
                footers+='<button href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="'+footer[t]["btnfun"]+'">'+footer[t]["btnname"]+'</button>'
            }
            t+=1;
        }
    }else{
        while(t<footer.length){
            if(footer[t]['noclose']==true || footer['noclose']=="true"){
                footers+='<button href="#!" class="waves-effect waves-green btn-flat timeset" onclick="'+footer[t]["btnfun"]+'">'+footer[t]["btnname"]+'</button>'
            }else if(footer[t]['notime']==true || footer['notime']=="true"){
                footers+='<button href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="'+footer[t]["btnfun"]+'">'+footer[t]["btnname"]+'</button>'
            }else{
                footers+='<button href="#!" class="modal-close waves-effect waves-green btn-flat disabled timeset" onclick="'+footer[t]["btnfun"]+'">'+footer[t]["btnname"]+'</button>'
            }
            t+=1;
        }
    }
    document.getElementById("botm_footer").innerHTML = footers;
    var time = new Date;
    var clsup = {
        node: node,
        endtime: parseInt(time.getTime()) + timeset * 1000,
        title: title,
        require: requires,
        footer: footers
    }
    localStorage.setItem("clsup", JSON.stringify(clsup)); //使课程可恢复
    let d = new Date();
    localStorage.setItem("timeset", parseInt(d.getTime()) + timeset * 1000)
    botm.open();
    ClsPicOn(node, localStorage.getItem("setting-pic"))
}

function clsrestart() { //课程继续
    try {
        var clsup = JSON.parse(localStorage.getItem("clsup"));
    } catch (err) {
        alert("哦呜，课程数据损坏！课白上了");
        localStorage.removeItem("clsup");
        location.reload();
    }
    let title = clsup["title"];
    let footer = clsup["footer"];
    let requires = clsup["require"];
    let node = clsup["node"]

    //开始弹出图片
    ctd = window.setInterval(ctdgo, 1000); //启动倒计时
    //向拟态框填充内容
    ClsPicOn(node, localStorage.getItem("setting-pic"));
    document.getElementById("botm_header").innerHTML = title;
    document.getElementById("botm_content").innerHTML = requires + "<p id=\"CountDown\">距离结束还有好多秒</p>";
    document.getElementById("botm_footer").innerHTML = footer;
    localStorage.setItem("timeset", clsup["endtime"]);
    botm.open();
}

function ctdgo() { //各类倒计时
    if (!localStorage.getItem("timeset")) {
        console.log("找不到定时点，退出");
        window.clearInterval(ctd);
        return;
    }
    if (localStorage.getItem("setting-notime")) {
        if (localStorage.getItem("setting-notime") == true || localStorage.getItem("setting-notime") == "true") {
            var allcls = document.getElementsByClassName("timeset"); //启用按钮
            var t = 0,
                a = allcls.length;
            while (t <= a && allcls[t] != undefined) {
                allcls[t].classList.remove("disabled");
                t += 1
            }
        }
    }

    let d = new Date(),
        left = Math.ceil((parseInt(localStorage.getItem("timeset")) - parseInt(d.getTime())) / 1000);
        try{
            document.getElementById("CountDown").innerHTML = "距离结束还有" + left + "秒"
        }
        catch(err){
            if(err instanceof TypeError){
                console.log("检测到类型错误："+err+"已清除计时器")
                window.clearInterval(ctd);
            }else{
                console.err(err)
            }
        }
        finally{
            if (left < 1) { //倒计时完成
                window.clearInterval(ctd);
                document.getElementById("CountDown").innerHTML = "";
                var allcls = document.getElementsByClassName("timeset"); //启用按钮
                var t = 0,
                    a = allcls.length;
                while (t <= a && allcls[t] != undefined) {
                    allcls[t].classList.remove("disabled");
                    t += 1
                }
            }
        }
}

function clsup(node, info) { //上课
    if (localStorage.getItem("clsup")) { //检查是否有课程继续
        alert("RBQ需要先上完现在的课程呢！");
        clsrestart()
        return;
    }
    //尝试动态加载，若失败提升升级浏览器

    console.info("远程服务器动态加载课程" + node)
    import('./lgame/' + node + '.js')
        .then(module => {
            module.clsup_e(info); //由该模块进行处理
        })
        .catch(err => {
            console.error(err.message);
            alert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
        });
}

function clsfin(node, info) { //课程弹框关闭
    localStorage.removeItem("timeset"); //移除倒计时、上课持续
    localStorage.removeItem("clsup");
    window.clearInterval(ctd);


    //尝试动态加载，若失败提升升级浏览器
    console.info("远程服务器动态加载课程" + node)
    import('./lgame/' + node + '.js')
        .then(module => {
            module.clsfin_e(info); //由该模块进行处理
        })
        .catch(err => {
            console.error(err.message);
            alert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
        });
}

function lgame(code) {//小游戏的游玩
    location.hash = "#c"
    location.hash = "#lgame/" + code;
    var now = "0";
    if (localStorage.getItem("lgame")) {
        let json = JSON.parse(localStorage.getItem("lgame"));
        if (typeof (json[code]) != "undefined") {
            now = json[code]
        }
    }
    clsup(code, now);
}